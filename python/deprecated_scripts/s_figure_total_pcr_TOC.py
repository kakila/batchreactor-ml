# vim: set tabstop=4
# s_figure_binary_pcr_maintenance.py
#!/usr/bin/env python3

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/.
#
# Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Mariane Schneider <mariane.sammel@meiru.ch>
# 07.03.2018

#' % PCR on the NH4 vs. pH signal
#' % Juan Pablo Carbajal, Mariane Schneider
#' % 01.02.2018

import numpy as np

import matplotlib.pyplot as plt
import matplotlib
import pdb
# matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.unicode'] = True
matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams['font.sans-serif'] = 'DejaVu Sans'
matplotlib.rcParams.update({'font.size': 14})

from sklearn.decomposition import PCA
from sklearn.linear_model import LinearRegression, ARDRegression
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import RepeatedKFold

from SBRdata.dataparser import SensorData, IOData
from pH_features import aeration_valley

#' ## Data load and pre-process
#' First load the data, filter NA values and select the output to regress.
#' In this case we use the NH4 value at the effluent.

savef = 1               # choose between 1 and 0 depening if the figure should
                        # be saved automatically or not
filename = '../data/180215_AI4_MW.csv'
filename2 = '../data/180215_pH CPS11D.1.csv'
performanceVar = 'TOC'    # NH4, TOC, DOC

pHData = SensorData(filename)
pHData2 = SensorData(filename2)
pH_all = pHData.interp_nan()
pH_all2 = pHData2.interp_nan()
t = pHData.time
t2 = pHData2.time
dt = t[1] - t[0]
dt2 = t2[1] - t2[0]

Ydata = IOData(filename)
Ydata2 = IOData(filename2)
# filter NA values in output
NH4_all = Ydata.TOC[:,1]
NH4_all2 = Ydata2.TOC[:,1]
nonan = np.logical_not(np.isnan(NH4_all))
nonan2 = np.logical_not(np.isnan(NH4_all2))
NH4_all = NH4_all[nonan]
NH4_all2 = NH4_all2[nonan2]
pH_all = pH_all[nonan,:]
pH_all2 = pH_all2[nonan,:]

# Filter inputs that do not show pH eration valley
# Select aeration phase
idx = np.nonzero(pHData.phase==4)[0]
idx2 = np.nonzero(pHData2.phase==4)[0]
_pH = pH_all[:,idx]
_pH2 = pH_all2[:,idx2]
w = np.int(2*np.floor(0.06/dt/2)+1)
w2 = np.int(2*np.floor(0.06/dt2/2)+1)
fp_valley_opt = {'order':np.copy(w)}
fp_valley_opt2 = {'order':np.copy(w2)}
s_valley_opt = {'freq':10}
s_valley_opt2 = {'freq':10}

timepHlist = []
timepHlist2 = []
novalley = np.array ([True]*pH_all.shape[0])
novalley2 = np.array([True]*pH_all2.shape[0])
valley = np.array([False]*pH_all.shape[0])
valley2 = np.array([False]*pH_all2.shape[0])
timeVal_python = []
timeVal_python2 = []
for i,s in enumerate(_pH): # s is all pH values for one cycle
    m, _ = aeration_valley(t, s, \
                           smooth_opt=s_valley_opt, \
                           findpeaks_opt=fp_valley_opt)
    if m:
        novalley[i] = False
        valley[i] = True
        timeVal_python.append(m)
for i,s in enumerate(_pH2): # s is all pH values for one cycle
    m2, _2 = aeration_valley(t, s, \
                           smooth_opt=s_valley_opt2, \
                           findpeaks_opt=fp_valley_opt2)
    if m2:
        novalley2[i] = False
        valley2[i] = True
        timeVal_python2.append(m2)

timeVal = np.array(timeVal_python)
timeVal2 = np.array(timeVal_python2)
NH4 = NH4_all
NH42 = NH4_all2
pH = pH_all
pH2 = pH_all2

NH4val = NH4_all[valley]
NH4val2 = NH4_all2[valley2]
pHval = pH_all[valley,:]
pHval2 = pH_all2[valley2,:]

#' ## PCR training
#' We extract PCs from the whole dataset
pca = PCA(n_components=7, svd_solver='full')
pca2 = PCA(n_components=7, svd_solver='full')
pca.fit(pH)
pca2.fit(pH2)

#' We choose the regression strategy
reg = ARDRegression(n_iter=int(1e3))
reg2 = ARDRegression(n_iter=int(1e3))

#' Then we find the best regressor coeficients by cross validation
rkf = RepeatedKFold(n_splits=5, n_repeats=50)
rkf2 = RepeatedKFold(n_splits=5, n_repeats=50)
coef = []
coef2 = []
for train_index, test_index in rkf.split(pH):
    pH_train, pH_test = pH[train_index,:], pH[test_index,:]
    y_train, y_test = NH4[train_index], NH4[test_index]

    X_train = pca.transform(pH_train)
    reg.fit (X_train, y_train)
    coef.append(reg.coef_)
for train_index2, test_index2 in rkf2.split(pH2):
    pH_train2, pH_test2 = pH2[train_index2,:], pH2[test_index2,:]
    y_train2, y_test2 = NH42[train_index2], NH42[test_index2]

    X_train2 = pca2.transform(pH_train2)
    reg2.fit (X_train2, y_train2)
    coef2.append(reg2.coef_)

reg.coef_ = np.median(coef, axis=0)
reg2.coef_2 = np.median(coef2, axis=0)

beta = pca.inverse_transform (reg.coef_)
beta2 = pca2.inverse_transform(reg2.coef_2)

#' Here we simply assess the quality of the regression using the dataset.

# Make predictions
# alternatively do pH_test.dot(M)
y_pred = reg.predict(pca.transform(pH))
y_pred2 = reg2.predict(pca2.transform(pH2))

print("Mean squared error: %.2f reference sensor"
      % mean_squared_error(NH4, y_pred))
print("Mean squared error: %.2f test sensor"
      % mean_squared_error(NH42, y_pred2))
# Explained variance score: 1 is perfect prediction
print('Variance score: %.2f reference sensor'
      % r2_score(NH4, y_pred))
print('Variance score: %.2f test sensor'
      % r2_score(NH42, y_pred2))

print('Number of Principal components for reference sensors: %.2f'
      % pca.n_components_)
print('Number of Principal components for reference sensors: %.2f'
      % pca2.n_components_)

# Figure 1 for abstract IWA, percentrage of aeration completed when ammonium
# valley occures.
fontsize = 14

fig, ax2 = plt.subplots(nrows=1, ncols=1)
ax2.plot(y_pred,NH4, 'bo', label='maintained sensor', markersize=6)
ax2.plot(y_pred2,NH42, 'gx', label='unmaintained sensor', markersize=12)

ax2.set_ylim(0,120)
ax2.set_ylabel('measured TOC$_{eff}$ [mg/L]', fontsize=fontsize)
ax2.set_xlabel('predicted TOC$_{eff}$ [mg/L]', fontsize=fontsize)
ax2.get_xaxis().tick_bottom()
ax2.set_xlim(ax2.get_ylim())
plt.title('(B) Cycles without ammonium valley', fontsize=18)
diag_line, = ax2.plot(ax2.get_xlim(), ax2.get_ylim(), ls="--", c=".3")

if savef == 1:
    fig.savefig('total_TOC.png')
    fig.savefig('total_TOC.eps', format='eps', dpi=1000)
else:
    pass
plt.tight_layout()
plt.figure(2)
plt.show()
