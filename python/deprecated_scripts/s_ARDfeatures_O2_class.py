# vim: set tabstop=4
# s_ARDfeatures_pH.py
#!/usr/bin/env python3
''' Carefull this program produces on certain Windows 7 version a power cut!!!

This is a test by Mariane to separate pH and dissolved oxygen (DO) skripts
Data load and pre-process
First load the data, filter NA values and select the output to regress.
In this case we use the NH4 value at the effluent.
'''
'''
 Copyright (C) 2018 Juan Pablo Carbajal
 Copyright (C) 2018 Maraine Yvonne Schneider

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Mariane Yvonne Schneider <m.sammel@meiru.ch>

############
## Imports
# built-ins
from SBRdata.dataparser import SensorData, IOData
from O2_features import respirogram
from pH_features import aeration_valley, aeration_climax

# 3rd party
import numpy as np
import matplotlib.pyplot as plt
import platform
import matplotlib
if platform.system() is not 'Windows':
    matplotlib.rcParams['text.usetex'] = True
else:
    pass
matplotlib.rcParams['text.latex.unicode'] = True
matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams['font.sans-serif'] = 'DejaVu Sans'

from sklearn.linear_model import ARDRegression
from sklearn.metrics import mean_squared_error, r2_score
from scipy.signal import argrelextrema
from scipy import signal

# TODO: do this OS robust
datapath = '../data/'
files = {'O2':'AI2_MW.csv'}
filename = datapath + files['O2']

## Load data and verify integrity of outputs
O2Data = SensorData(filename)
O2 = O2Data.interp_nan()
Ydata = IOData(filename)

# filter NA values in output and discard the pH cycles without io measurements
NH4 = Ydata.NH4[:,1]
nonan = np.logical_not(np.isnan(NH4))
NH4 = NH4[nonan]
O2 = O2[nonan,:]

# if this step is included only the NH4+N concentrations above 0.3 are included
above = np.greater(NH4,0.5)
NH4 = NH4[above]
O2 = O2[above,:]

if 'reg' not in vars():
    # Regression with ARD and check the weight of eacht time step
    reg = ARDRegression (n_iter=int(100), compute_score=True)
    reg.fit(O2, NH4)

    # Order features with decreasing weight
    o = np.argsort(np.abs(reg.coef_))[::-1]
    idx = o[:16]

# Regress with selected Features
reg_s = ARDRegression (n_iter=int(300))
reg_s.fit(O2[:,idx], NH4)
y_pred = reg_s.predict(O2[:,idx])

print("Mean squared error: %.2f "
      % mean_squared_error(NH4, y_pred))
# Explained variance score: 1 is perfect prediction
print('Variance score: %.2f '
      % r2_score(NH4, y_pred))


n = 1
fontsize = 14
#+ caption= "ARD feature selection"
if 'pH' in files:
    fig, ax1 = plt.subplots(nrows=1, ncols=1)
    ax1.plot(y_pred, NH4, 'bo', label='maintained pH prediction')
    ax1.set_ylabel('measured NH$_4$-N$_{eff}$ [mg$_N$/L]', fontsize=fontsize)
    ax1.set_xlabel('predicted NH$_4$-N$_{eff}$ [mg$_N$/L]', fontsize=fontsize)
    ax1.legend(loc=2, numpoints=1)
    ax1.set_ylim(0,50)
    ax1.set_xlim(0,50)
    # ax1.axhline(y=0.5,xmin=0,xmax=2.5,c="black",linewidth=1,linestyle='--',zorder=0)
    plt.title('prediction with pH', fontsize=18)
    plt.tight_layout()
    plt.figure(n)
    plt.show()

    n = n + 1

    if 'O2' in files:
        #+ caption= "ARD feature selection"
        plt.figure(n)
        plt.clf()
        _n_ = np.arange(len(reg.coef_))
        plt.bar(_n_, reg.coef_)
        plt.ylabel('Weight in the model')
        name = np.array(['pH\_t\_max', 'pH\_max','O2\_R-', 'O2\_R+',\
            'e\_sz\_O2\_R-','e\_sz\_O2\_R+'])
        plt.xticks(_n_, name)
        plt.show()
        n = n + 1
        print('O2 and pH')
    else:
        print('only pH')
else:
    if 'O2' in files:

        plt.figure(n)
        plt.clf()
        plt.plot(NH4,'o', label='data')
        plt.plot(y_pred,'x', label='pred.')
        plt.ylabel('NH4')
        plt.xlabel('Cycle')
        plt.legend()
        plt.show()
        n = n + 1
        print('only O2')

plt.ion()
plt.figure(1)
plt.clf()
plt.subplot(2,1,1)
plt.plot(O2Data.time, reg.coef_)
plt.ylabel('Weight of the model')
plt.subplot(2,1,2)

s = np.abs(reg.coef_[idx])
s = 36 * s / s.max() + 16
plt.scatter(O2Data.time[idx], np.mean(O2[:,idx],axis=0), s=s, alpha=0.9)
#plt.autoscale(enable=False, axis='y', tight=True)
plt.ylim(0,3)
plt.autoscale(enable=False, axis='x', tight=True)
plt.plot(O2Data.time, O2.T,'-', linewidth=0.5)
plt.ylabel('O2')
plt.show()

#+ caption= "Regression results."
plt.figure(2)
plt.clf()
plt.plot(NH4,'o', label='data')
plt.plot(y_pred,'x', label='pred.')
plt.ylabel('NH4')
plt.xlabel('Cycle')
plt.legend()
plt.show()
