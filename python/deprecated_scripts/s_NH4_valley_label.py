# vim: set tabstop=4
# s_NH4_valley_label.py
#!/usr/bin/env python3

""" Manual labling of the dataset."""

# Copyright (C) 2018 Juan Pablo Carbajal
# Copyright (C) 2018 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Mariane Yvonne Schneider <myschneider@meiru.ch>
# 21.02.2019

import platform

from datetime import datetime

import numpy as np

import matplotlib
import matplotlib.pyplot as plt

from SBRdata.utils import (
    Result,
    sensorsdict,
    processrawdata,
    fscore,
    contingencytable
)
from SBRfeatures.pH_features import aeration_valley
from SBRfeatures.basic_funcs import smooth
from SBRplots.point_features import (
    feature_vs_value,
    feature_signal_and_output
)

matplotlib.rcParams['text.latex.unicode'] = True
matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams['font.sans-serif'] = 'DejaVu Sans'
matplotlib.rcParams.update({'font.size': 12})
if platform.system() != 'Windows':
    matplotlib.rcParams['text.usetex'] = True
    plt.ion()
else:
    pass

# Configure the size of the legend
LEGENDFMT = dict(bbox_to_anchor=(0, 1.02, 1.3, 0.3),
    loc='lower left', mode='expand', ncol=3, fontsize='small')

# Globals to pass values to plot functions
NH4 = np.array([])
NH4ISLOW = np.array([])
NH4THRESHOLD = 1
CYCLENAMES = np.array([])

def plot_all(time, svalue, O2time, O2value, phase, date, dateO2):
    fig, axes = plt.subplots(nrows=1, ncols=1)
    axes.set_ylabel('NH4 [mg/L]')
    axes.set_xlabel('Time')

    axes.plot(time, svalue, label='NH4 ISE')
    axes.plot(O2time, O2value, label='dissolved oxygen')
    axes.plot(time, phase, label='Aeration phase')
    axes.axhline(y=0.58,linestyle='--', color='red', label='offset ISE')
    axes.set_title("NH4: {0}, O2: {1}".format(date, dateO2))
    axes.legend()
    return fig, axes

if __name__ == '__main__':
    import csv
    sensorsource = sensorsdict(stype='ISE')
    sensorO2 = sensorsdict(stype='DO')
    NH4, nonan, sensor = processrawdata(filesdict=sensorsource)
    _, _, sensorO2 = processrawdata(filesdict=sensorO2)
    NH4ISLOW = (NH4 <= NH4THRESHOLD)
    keylist = []
    labeldict = {}

    for sname, s in sensorO2.items():
        print('** Sensor type: %s' %sname)
        O2CYCLENAMES = np.array(s.cycle_names())
        O2value = s.sensor
        O2time = s.time
        O2phase = s.phase
        O2date = s.datetime

    for sname, s in sensor.items():
        print('** Sensor type: %s' %sname)

        CYCLENAMES = np.array(s.cycle_names())
        svalue = s.sensor
        t = s.time
        phase = s.phase
        air = (s.phase == 4)
        date = s.datetime

        for vnum, values in enumerate(svalue):
            if CYCLENAMES[vnum] == O2CYCLENAMES[vnum]:
                fig, axes = plot_all(t, svalue[vnum], O2time, O2value[vnum],
                                     air, date[vnum], O2date[vnum])
                plt.pause(1)
                key = input('q = quit; 0 if NH4 > 0.6, f
                loat if NH4 < 0.6, nan if unclear:')
                key = 1

                plt.close()
                if key == 'q':
                    with open("output.csv", "w") as f:
                        writer = csv.writer(f, delimiter=';', lineterminator='\n')
                        for key, value in labeldict.items():
                            writer.writerow([key, value])
                    quit()
                labeldict[date[vnum]] = key
                keylist.append(labeldict)
            else:
                raise ValueError('Note same cycle. NH4-N from {}, DO from {}'.format(date[vnum], O2date[vnum]))

    with open("output.csv", "w") as f:
        writer = csv.writer(f, delimiter=';', lineterminator='\n')
        for key, value in labeldict.items():
            writer.writerow([key, value])
