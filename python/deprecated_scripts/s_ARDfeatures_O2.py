# vim: set tabstop=4
# s_ARDfeatures_pH.py
#!/usr/bin/env python3
''' This is a test by Mariane to separate pH and dissolved oxygen (DO) skripts
Data load and pre-process
First load the data, filter NA values and select the output to regress.
In this case we use the NH4 value at the effluent.
'''

############
## Imports
# built-ins
from SBRdata.dataparser import SensorData, IOData
from O2_features import respirogram
from pH_features import aeration_valley, aeration_climax

# 3rd party
import numpy as np
import matplotlib.pyplot as plt
import platform
if platform.system() is not 'Windows':
    import matplotlib
    matplotlib.rcParams['text.usetex'] = True
    matplotlib.rcParams['text.latex.unicode'] = True
    matplotlib.rcParams['font.family'] = 'sans-serif'
    matplotlib.rcParams['font.sans-serif'] = 'DejaVu Sans'

from sklearn.linear_model import ARDRegression
from sklearn.metrics import mean_squared_error, r2_score
from scipy.signal import argrelextrema
from scipy import signal

# user
############
__author__ = "Juan Pablo Carbajal, and Mariane Yvonne Schneider"
__copyright__ = "Copyright (C) 2018 - Juan Pablo Carbajal, Maraine Yvonne Schneider"
__credits__ = ["Juan Pablo Carbajal", "Mariane Yvonne Schneider"]
__license__ = """This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>."""
__version__ = "0.0.1"
__maintainer__ = "Juan Pablo Carbajal", "Mariane Yvonne Schneider"
__email__ = "ajuanpi+dev@gmailcom", "m.sammel@meiru.ch"
__status__ = "Development"
__date__ = "25.04.2018"

# TODO: do this OS robust
datapath = '../data/'
files = {'pH':'AI4_MW.csv', 'O2':'AI2_MW.csv'}
# files = {'O2':'AI2_MW.csv'}
# files = {'pH':'AI4_MW.csv'}

## Load data and verify integrity of outputs
sensor = dict()
_NH4 = np.array([])
for signal, filenam in files.items():
    filename = datapath + filenam
    print('Processing signal {} from {}'.format(signal, filename))

    Ydata = IOData(filename)
    if not _NH4.size > 0:
        _NH4 = Ydata.NH4[:,1]

        # filter NA values in output
        nonan = np.isfinite(_NH4)
        _NH4 = _NH4[nonan]

    # Check allfiles have the same output values
    if not np.allclose (_NH4, Ydata.NH4[nonan,1]):
        raise ValueError('Output signals differ!')

    sensor[signal] = SensorData(filename)

if 'pH' in sensor:
    ## Filter inputs that do not show pH aeration valley
    ## Select aeration phase
    pH_aer = np.nonzero(sensor['pH'].phase==4)[0]
    _pH = sensor['pH'].interp_nan()[nonan,:][:,pH_aer]
    pH_t = sensor['pH'].time[pH_aer]
    pH_t = (pH_t - pH_t[0])/(pH_t[-1] - pH_t[0])
    dt = pH_t[1] - pH_t[0]

    ## Compute amonia valley
    fp_valley_opt = {'order':5}
    s_valley_opt = {'freq':5}

    novalley = np.array ([True]*_pH.shape[0])
    for i,s in enumerate(_pH): # s is all pH values for one cycle
        m, s_smooth = aeration_valley(pH_t, s, \
                               smooth_opt=s_valley_opt, \
                               findpeaks_opt=fp_valley_opt)
        if m:
            novalley[i] = False

    print("Number of signals with amonia valley: {}/{}".format(\
        np.sum(np.logical_not(novalley)), _pH.shape[0]))

    NH4 = _NH4[novalley]
    pH = _pH[novalley,:]

    #' ## Extract features
    from scipy.signal import argrelextrema
    fp_climax_opt = {'finder':argrelextrema, 'comparator':np.greater_equal,
        'order':5}
    s_climax_opt = {'freq':10}

    pH_M = np.zeros ([pH.shape[0],2])
    for i,s in enumerate(pH): # s is all pH values for one cycle
        val, s_smooth = aeration_climax(pH_t, s, \
                smooth_opt=s_climax_opt, findpeaks_opt=fp_climax_opt)
        pH_M[i,:] = np.array(val)

    N = pH.shape[0]
    X = np.zeros([N,len(sensor)*2])
    X[:,0:2] = pH_M

if 'O2' in sensor:
    O2_aer = np.nonzero(sensor['O2'].phase==4)[0]
    _O2 = sensor['O2'].interp_nan()[nonan,:][:,O2_aer]
    O2_t = sensor['O2'].time[O2_aer]
    O2_t = (O2_t - O2_t[0])/(O2_t[-1] - O2_t[0])
    if 'pH' in sensor:
        NH4 = _NH4[novalley]
        O2 = _O2[novalley,:]
    else:
        O2 = _O2
        NH4 = _NH4

    O2_R = np.zeros ([O2.shape[0],2])
    for i,s in enumerate(O2): # s is all O2 values for one cycle
        Rm, Rp, idx = respirogram (O2_t, s)

        O2_R[i,0] = np.mean(Rm[idx[0::2]])
        O2_R[i,1] = np.mean(Rp[idx[1::2]])

    if 'pH' in sensor:
        X[:,2:4] = O2_R
    else:
        O2 = _O2
        N = O2.shape[0]
        X = np.zeros([N,len(sensor)*2])
        X[:,0:2] = O2_R

scale = lambda x: (x - np.mean(x,axis=0)) / np.std(x,axis=0) # zscore
X = scale(X)

reg = ARDRegression (n_iter=int(1e3), compute_score=True)
reg.fit(X, NH4)
y_pred = reg.predict(X)

print("Mean squared error: %.2f "
      % mean_squared_error(NH4, y_pred))
# Explained variance score: 1 is perfect prediction
print('Variance score: %.2f '
      % r2_score(NH4, y_pred))

n = 1
fontsize = 14
#+ caption= "ARD feature selection"
if 'pH' in sensor:
    fig, ax1 = plt.subplots(nrows=1, ncols=1)
    ax1.plot(y_pred, NH4, 'bo', label='maintained pH prediction')
    ax1.set_ylabel('measured NH$_4$-N$_{eff}$ [mg$_N$/L]', fontsize=fontsize)
    ax1.set_xlabel('predicted NH$_4$-N$_{eff}$ [mg$_N$/L]', fontsize=fontsize)
    ax1.legend(loc=2, numpoints=1)
    ax1.set_ylim(0,50)
    ax1.set_xlim(0,50)
    # ax1.axhline(y=0.5,xmin=0,xmax=2.5,c="black",linewidth=1,linestyle='--',zorder=0)
    plt.title('prediction with pH', fontsize=18)
    plt.tight_layout()
    plt.figure(n)
    plt.show()
    # plots which are interesting when only the pH is evaluated
    # plt.figure(n)
    # plt.clf()
    # plt.plot(y_pred, NH4)
    # plt.show()
    # plt.ion()
    # plt.figure(n)
    # plt.clf()
    # plt.subplot(2,1,1)
    # plt.plot(X.time, reg.coef_)
    # plt.ylabel('Weight of the model')
    # plt.subplot(2,1,2)
    #
    # s = np.abs(reg.coef_[idx])
    # s = 36 * s / s.max() + 16
    # plt.scatter(X.time[idx], np.mean(pH[:,idx],axis=0), s=s, alpha=0.9)
    # #plt.autoscale(enable=False, axis='y', tight=True)
    # plt.ylim(7.25,8.3)
    # plt.autoscale(enable=False, axis='x', tight=True)
    # plt.plot(X.time, pH.T,'-', linewidth=0.5)
    # plt.ylabel('pH')
    # plt.show()
    n = n + 1

    if 'O2' in sensor:
        #+ caption= "ARD feature selection"
        plt.figure(n)
        plt.clf()
        _n_ = np.arange(len(reg.coef_))
        plt.bar(_n_, reg.coef_)
        plt.ylabel('Weight in the model')
        name = np.array(['pH\_t\_max', 'pH\_max','O2\_R-', 'O2\_R+',\
            'e\_sz\_O2\_R-','e\_sz\_O2\_R+'])
        plt.xticks(_n_, name)
        plt.show()
        n = n + 1
        print('O2 and pH')
    else:
        print('only pH')
else:
    if 'O2' in sensor:
#         plt.figure(3)
#         plut.clf()
#         plt.plot()
#         plt.subplot(2,1,1)
#         plt.plot(pHData.time, reg.coef_)
#         # plt.ylabel('Weight of the model')
#         # plt.subplot(2,1,2)
#         #
#         # s = np.abs(reg.coef_[idx])
#         # s = 36 * s / s.max() + 16
#         # plt.scatter(pHData.time[idx], np.mean(pH[:,idx],axis=0), s=s, alpha=0.9)
#         # #plt.autoscale(enable=False, axis='y', tight=True)
#         # plt.ylim(7.25,8.3)
#         # plt.autoscale(enable=False, axis='x', tight=True)
#         # plt.plot(pHData.time, pH.T,'-', linewidth=0.5)
#         # plt.ylabel('pH')
#         # plt.show()
        #+ caption= "Regression results."
        plt.figure(n)
        plt.clf()
        plt.plot(NH4,'o', label='data')
        plt.plot(y_pred,'x', label='pred.')
        plt.ylabel('NH4')
        plt.xlabel('Cycle')
        plt.legend()
        plt.show()
        n = n + 1
        print('only O2')
