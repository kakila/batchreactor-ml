# -*- coding: utf-8 -*-
# vim: set tabstop=4
# s_figure_maintained_vs_unmaintained.py
#!/usr/bin/env python3

# Copyright (C) 2018 Juan Pablo Carbajal
# Copyright (C) 2018 Maraine Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Plot pH raw data"""

import matplotlib.pyplot as plt
import platform
import matplotlib
if platform.system() is not 'Windows':
    matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.unicode'] = True
matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams['font.sans-serif'] = 'DejaVu Sans'
matplotlib.rcParams.update({'font.size': 20})

from SBRdata.dataparser import SensorData

savef = 0

filename ='../data/mV ORP CPS12D.1.csv'
filename2 ='../data/mV ORP CPS12D.2.csv'

maintainedData = SensorData(filename)
maintained = maintainedData.sensor
t = maintainedData.time

unmaintainedData = SensorData(filename2)
unmaintained = unmaintainedData.sensor
t = unmaintainedData.time

fig = plt.figure(1)
plt.plot(t, maintained[0], label='gewartet', color = [13/256,114/256,186/256], linewidth = 4)
plt.plot(t, unmaintained[0], label='ungewartet', color = [123/256,156/256,209/256], linewidth = 4)
plt.xlabel('Zykluszeit vergangen')
plt.ylabel('pH [-]')
plt.legend(loc='best')
plt.title('20.05.2017')

plt.ylim(7.3,7.9)
if savef == 1:
    fig.savefig('../figures/pH_t0.png')
    fig.savefig('../figures/pH_t0.eps', format='eps', dpi=1000)
else:
    pass
plt.tight_layout()
plt.show()

plt.figure(2)
for i in maintained:
    plt.plot(t, i)
plt.xlabel('Zykluszeit vergangen')
plt.ylabel('pH [-]')
# plt.tight_layout()
if savef == 1:
    fig_1.savefig('../figures/pH_maintained_tall.png')
    fig_1.savefig('../figures/pH_maintained_tall.eps', format='eps', dpi=1000)
else:
    pass
plt.show()

plt.figure(3)
for i in unmaintained:
    plt.plot(t, i)
plt.xlabel('Zykluszeit vergangen')
plt.ylabel('pH [-]')
if savef == 1:
    fig_1.savefig('../figures/pH_unmaintained_tall.png')
    fig_1.savefig('../figures/pH_unmaintained_tall.eps', format='eps', dpi=1000)
else:
    pass
# plt.tight_layout()
plt.show()
