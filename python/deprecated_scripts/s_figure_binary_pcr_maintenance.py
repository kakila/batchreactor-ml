# vim: set tabstop=4
# s_figure_binary_pcr_maintenance.py
#!/usr/bin/env python3
''' plots to show differences in prediction between maintained and unmaintained
pH sensors.
'''

############
## Imports
# built-ins
from SBRdata.dataparser import SensorData, IOData
from O2_features import respirogram
from pH_features import aeration_valley, aeration_climax

# 3rd party
import numpy as np
import matplotlib.pyplot as plt
import platform
if platform.system() is not 'Windows':
    import matplotlib
    matplotlib.rcParams['text.usetex'] = True
    matplotlib.rcParams['text.latex.unicode'] = True
    matplotlib.rcParams['font.family'] = 'sans-serif'
    matplotlib.rcParams['font.sans-serif'] = 'DejaVu Sans'

from sklearn.linear_model import ARDRegression
from sklearn.metrics import mean_squared_error, r2_score
from scipy.signal import argrelextrema
from scipy import signal

import numpy as np

import matplotlib.pyplot as plt
import matplotlib
import pdb
# matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.unicode'] = True
matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams['font.sans-serif'] = 'DejaVu Sans'
matplotlib.rcParams.update({'font.size': 14})

from sklearn.decomposition import PCA
from sklearn.linear_model import LinearRegression, ARDRegression
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import RepeatedKFold

from SBRdata.dataparser import SensorData, IOData
from pH_features import aeration_valley

#' ## Data load and pre-process
#' First load the data, filter NA values and select the output to regress.
#' In this case we use the NH4 value at the effluent.

savef = 1               # choose between 1 and 0 depening if the figure should
                        # be saved automatically or not
fontsize = 14           # Larger Fontsize for plots

datapath = '../data/'
files = {'pH_1':'AI4_MW.csv', 'pH_2':'pH CPS11D.1.csv'}

sensor = dict()
_NH4 = np.array([])
for signal, filenam in files.items():
    filename = datapath + filenam
    print('Processing signal {} from {}'.format(signal, filename))

    Ydata = IOData(filename)
    if not _NH4.size > 0:
        _NH4 = Ydata.NH4[:,1]

        # filter NA values in output
        nonan = np.isfinite(_NH4)
        _NH4 = _NH4[nonan]

    # Check allfiles have the same output values
    if not np.allclose (_NH4, Ydata.NH4[nonan,1]):
        raise ValueError('Output signals differ!')

    sensor[signal] = SensorData(filename)

## Filter inputs that do not show pH aeration valley
## Select aeration phase
pH_aer_1 = np.nonzero(sensor['pH_1'].phase==4)[0]
_pH_1 = sensor['pH_1'].interp_nan()[nonan,:][:,pH_aer_1]
pH_t_1 = sensor['pH_1'].time[pH_aer_1]
pH_t_1 = (pH_t_1 - pH_t_1[0])/(pH_t_1[-1] - pH_t_1[0])
dt_1 = pH_t_1[1] - pH_t_1[0]

pH_aer_2 = np.nonzero(sensor['pH_2'].phase==4)[0]
_pH_2 = sensor['pH_2'].interp_nan()[nonan,:][:,pH_aer_2]
pH_t_2 = sensor['pH_2'].time[pH_aer_2]
pH_t_2 = (pH_t_2 - pH_t_2[0])/(pH_t_2[-1] - pH_t_2[0])
dt_2 = pH_t_2[1] - pH_t_2[0]

## Compute amonia valley for pH sensor 1 and 2
fp_valley_opt = {'order':5}
s_valley_opt = {'freq':5}

# Maintained sensor
novalley_1 = np.array ([True]*_pH_1.shape[0])
valley_1 = np.array ([False]*_pH_1.shape[0])
timeVal_python_1 = []
for i,s in enumerate(_pH_1): # s is all pH values for one cycle
    m, s_smooth = aeration_valley(pH_t_1, s, \
                           smooth_opt=s_valley_opt, \
                           findpeaks_opt=fp_valley_opt)
    if m:
        novalley_1[i] = False
        valley_1[i] = True
        timeVal_python_1.append(m)

print("Number of signals with amonia valley (maintained): {}/{}".format(\
    np.sum(np.logical_not(novalley_1)), _pH_1.shape[0]))

timeVal_1 = np.array(timeVal_python_1)
NH4_1 = _NH4[novalley_1]
pH_1 = _pH_1[novalley_1,:]
NH4val_1 = _NH4[valley_1]
pHval_1 = _pH_1[valley_1,:]

# Unmaintained sensor
novalley_2 = np.array ([True]*_pH_2.shape[0])
valley_2 = np.array ([False]*_pH_2.shape[0])
timeVal_python_2 = []
novalley_2 = np.array ([True]*_pH_2.shape[0])
for i,s in enumerate(_pH_2): # s is all pH values for one cycle
    m, s_smooth = aeration_valley(pH_t_2, s, \
                           smooth_opt=s_valley_opt, \
                           findpeaks_opt=fp_valley_opt)
    if m:
        novalley_2[i] = False
        valley_2[i] = True
        timeVal_python_2.append(m)

print("Number of signals with amonia valley (unmaintained): {}/{}".format(\
    np.sum(np.logical_not(novalley_2)), _pH_2.shape[0]))

timeVal_2 = np.array(timeVal_python_2)
NH4_2 = _NH4[novalley_2]
pH_2 = _pH_2[novalley_2,:]
NH4val_2 = _NH4[valley_2]
pHval_2 = _pH_2[valley_2,:]

#' ## PCR training
#' We extract PCs from the whole dataset
pca_1 = PCA(n_components=7, svd_solver='full')
pca_2 = PCA(n_components=7, svd_solver='full')
pca_1.fit(pH_1)
pca_2.fit(pH_2)

#' We choose the regression strategy
reg_1 = ARDRegression(n_iter=int(1e3))
reg_2 = ARDRegression(n_iter=int(1e3))

#' Then we find the best regressor coeficients by cross validation
rkf_1 = RepeatedKFold(n_splits=5, n_repeats=50)
rkf_2 = RepeatedKFold(n_splits=5, n_repeats=50)
coef_1 = []
coef_2 = []
for train_index_1, test_index_1 in rkf_1.split(pH_1):
    pH_train_1, pH_test_1 = pH_1[train_index_1,:], pH_1[test_index_1,:]
    y_train_1, y_test_1 = NH4_1[train_index_1], NH4_1[test_index_1]

    X_train_1 = pca_1.transform(pH_train_1)
    reg_1.fit(X_train_1, y_train_1)
    coef_1.append(reg_1.coef_)

for train_index_2, test_index_2 in rkf_2.split(pH_2):
    pH_train_2, pH_test_2 = pH_2[train_index_2,:], pH_2[test_index_2,:]
    y_train_2, y_test_2 = NH4_2[train_index_2], NH4_2[test_index_2]

    X_train_2 = pca_2.transform(pH_train_2)
    reg_2.fit(X_train_2, y_train_2)
    coef_2.append(reg_2.coef_)

reg_1.coef_ = np.median(coef_1, axis=0)
reg_2.coef_ = np.median(coef_2, axis=0)

beta_1 = pca_1.inverse_transform (reg_1.coef_)
beta_2 = pca_2.inverse_transform(reg_2.coef_)

#' Here we simply assess the quality of the regression using the dataset.

# Make predictions
# alternatively do pH_test.dot(M)
y_pred_1 = reg_1.predict(pca_1.transform(pH_1))
y_pred_2 = reg_2.predict(pca_2.transform(pH_2))

print("Mean squared error: %.2f reference sensor"
      % mean_squared_error(NH4_1, y_pred_1))
print("Mean squared error: %.2f test sensor"
      % mean_squared_error(NH4_2, y_pred_2))
# Explained variance score: 1 is perfect prediction
print('Variance score: %.2f reference sensor'
      % r2_score(NH4_1, y_pred_1))
print('Variance score: %.2f test sensor'
      % r2_score(NH4_2, y_pred_2))

print('Number of Principal components for reference sensors: %.2f'
      % pca_1.n_components_)
print('Number of Principal components for reference sensors: %.2f'
      % pca_2.n_components_)

# Figure 1 for abstract IWA, percentrage of aeration completed when ammonium
# valley occures.
fig_1, ax_1 = plt.subplots(nrows=1, ncols=1)

ax_1.plot(timeVal_1[:,0]*100, NH4val_1, 'bo', label='maintained pH sensor', markersize=6)
ax_1.plot(timeVal_2[:,0]*100, NH4val_2, 'gx', label='unmaintained pH sensor', markersize=12)
ax_1.set_ylabel('measured NH$_4$-N$_{eff}$ [mg$_N$/L]', fontsize=fontsize)
ax_1.set_xlabel('% ' 'of aeration completed when ammonium valley identified', fontsize=fontsize)
ax_1.legend(loc=2, numpoints=1)
ax_1.set_ylim(0,25)
ax_1.set_xlim(0,100)
ax_1.axhline(y=0.5,xmin=0,xmax=2.5,c="black",linewidth=1,linestyle='--',zorder=0)
plt.title('(A) Cycles with ammonium valley', fontsize=18)
if savef == 1:
    fig_1.savefig('../figures/binary1_maintenance.png')
    fig_1.savefig('../figures/binary1_maintenance.eps', format='eps', dpi=1000)
else:
    pass
plt.tight_layout()
plt.figure(1)
plt.show()

fig_2, ax_2 = plt.subplots(nrows=1, ncols=1)
ax_2.plot(y_pred_1, NH4_1, 'bo', label='maintained sensor', markersize=6)
ax_2.plot(y_pred_2,NH4_2, 'gx', label='unmaintained sensor', markersize=12)
ax_2.set_ylim(0,50)
ax_2.set_ylabel('measured NH$_4$-N$_{eff}$ [mg$_N$/L]', fontsize=fontsize)
ax_2.set_xlabel('predicted NH$_4$-N$_{eff}$ [mg$_N$/L]', fontsize=fontsize)
ax_2.get_xaxis().tick_bottom()
ax_2.set_xlim(ax_2.get_ylim())
plt.title('(B) Cycles without ammonium valley', fontsize=18)
diag_line, = ax_2.plot(ax_2.get_xlim(), ax_2.get_ylim(), ls="--", c=".3")
if savef == 1:
    fig_2.savefig('../figures/binary2_maintenance.png')
    fig_2.savefig('../figures/binary2_maintenance.eps', format='eps', dpi=1000)
else:
    pass
plt.tight_layout()
plt.figure(2)
plt.show()

# FIXME both plots show several
