# vim: set tabstop=4
# s_ENGfeatures_regress.py
#!/usr/bin/env python3

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/.
#
# Juan Pablo Carbajal <ajuanpi+dev@gmailcom>
# 01.02.2018

#' % Regression using engineered features
#' % Juan Pablo Carbajal <ajuanpi+dev@gmailcom>
#' % 12.03.2018

import numpy as np

import matplotlib.pyplot as plt
plt.close('all')

import platform
if platform.system() is not 'Windows':
    import matplotlib
    matplotlib.rcParams['text.usetex'] = True
    matplotlib.rcParams['text.latex.unicode'] = True
    matplotlib.rcParams['font.family'] = 'sans-serif'
    matplotlib.rcParams['font.sans-serif'] = 'DejaVu Sans'

from sklearn.linear_model import ARDRegression
from sklearn.metrics import mean_squared_error, r2_score

from SBRdata.dataparser import SensorData, IOData
from pH_features import aeration_valley, aeration_climax
from O2_features import respirogram

# TODO: do this OS robust
datapath = '../data/'
files = {'pH':'180215_AI4_MW.csv', 'O2':'180130_AI2_MW.csv'}

#' ## Load data and verify integrity of outputs
sensor = dict()
_NH4 = np.array([])
for signal, filenam in files.items():
    filename = datapath + filenam
    print('Processing signal {} from {}'.format(signal, filename))

    Ydata = IOData(filename)
    if not _NH4.size > 0:
        _NH4 = Ydata.NH4[:,1]

        # filter NA values in output
        nonan = np.isfinite(_NH4)
        _NH4 = _NH4[nonan]

    # Check allfiles have the same output values
    if not np.allclose (_NH4, Ydata.NH4[nonan,1]):
        raise ValueError('Output signals differ!')

    sensor[signal] = SensorData(filename)

#' ## Filter inputs that do not show pH aeration valley
## Select aeration phase
pH_aer = np.nonzero(sensor['pH'].phase==4)[0]
_pH = sensor['pH'].interp_nan()[nonan,:][:,pH_aer]
pH_t = sensor['pH'].time[pH_aer]
pH_t = (pH_t - pH_t[0])/(pH_t[-1] - pH_t[0])
dt = pH_t[1] - pH_t[0]

## Compute amonia valley
fp_valley_opt = {'order':5}
s_valley_opt = {'freq':5}

novalley = np.array ([True]*_pH.shape[0])
for i,s in enumerate(_pH): # s is all pH values for one cycle
    m, s_smooth = aeration_valley(pH_t, s, \
                           smooth_opt=s_valley_opt, \
                           findpeaks_opt=fp_valley_opt)
    if m:
        novalley[i] = False
    # uncomment here to see the signals
#    plt.figure(1)
#    plt.clf()
#    plt.plot(pH_t, s, label='signal')
#    plt.plot(pH_t, s_smooth, label='smoothed signal')
#    if m:
#        plt.plot(*m, 'go', label='valley')
#    plt.xlabel('time')
#    plt.ylabel('value')
#    plt.autoscale(enable=True, axis='x', tight=True)
#    plt.show()

print("Number of signals with amonia valley: {}/{}".format(\
    np.sum(np.logical_not(novalley)), _pH.shape[0]))

## Select data without amoinia valley
NH4 = _NH4[novalley]
pH = _pH[novalley,:]

O2_aer = np.nonzero(sensor['O2'].phase==4)[0]
_O2 = sensor['O2'].interp_nan()[nonan,:][:,O2_aer]
O2_t = sensor['O2'].time[O2_aer]
O2_t = (O2_t - O2_t[0])/(O2_t[-1] - O2_t[0])
O2 = _O2[novalley,:]

#' ## Extract features
from scipy.signal import argrelextrema
fp_climax_opt = {'finder':argrelextrema, 'comparator':np.greater_equal,
    'order':5}
s_climax_opt = {'freq':10}

pH_M = np.zeros ([pH.shape[0],2])
for i,s in enumerate(pH): # s is all pH values for one cycle
    val, s_smooth = aeration_climax(pH_t, s, \
            smooth_opt=s_climax_opt, findpeaks_opt=fp_climax_opt)
    pH_M[i,:] = np.array(val)
    # uncomment here to see the signals
#    plt.figure(1)
#    plt.clf()
#    plt.plot(pH_t, s, label='signal')
#    plt.plot(pH_t, s_smooth, label='smoothed signal')
#    plt.plot(*val, 'go', label='climax')
#    plt.xlabel('time')
#    plt.ylabel('value')
#    plt.autoscale(enable=True, axis='x', tight=True)
#    plt.show()

plt.ion()
plt.figure(1)
plt.clf()
ax = [plt.subplot(2,2,i+1) for i in range(4)]
plt.show()

from scipy.stats import iqr
O2_R = np.zeros ([O2.shape[0],4])
for i,s in enumerate(O2): # s is all O2 values for one cycle
    Rm, Rp, cuts, spl = respirogram (O2_t, s, noise=5e-2*np.std(s))

    # mean values of the derivatives with(pos) and without(neg) aeration
    neg = []
    pos = []
    for i,s in enumerate(cuts[0:-2]):
        if not np.isfinite(Rm[s]):
            tmp = Rp[s:cuts[i+1]]
            pos.append(np.mean(tmp))
        else:
            tmp = Rm[s:cuts[i+1]]
            neg.append(np.mean(tmp))
        assert np.isfinite(neg).all()
        assert np.isfinite(pos).all()
    neg = np.array(neg)
    neg = neg[neg<0]
    pos = np.array(pos)
    pos = pos[pos>0]
    xneg = np.linspace(0,1,len(neg))
    xpos = np.linspace(0,1,len(pos))
    O2_R[i,[2,0]] = np.polyfit(xneg, neg, deg=1)
    O2_R[i,[3,1]] = np.polyfit(xpos, pos, deg=1)

    # summary statistic of the whole derviatives with(pos) and without(neg) aeration
#    neg = Rm[Rm<0]
#    pos = Rp[Rp>0]
#    O2_R[i,4] = -np.sqrt(-np.median(neg)) # mean negative values
#    O2_R[i,5] = np.median(neg) # mean negative values
#    O2_R[i,6] = np.median(pos) # mean negative values
#    O2_R[i,7] = np.sqrt(np.median(pos)) # mean negative values
#    O2_R[i,4] = iqr(neg) # iqr negative values

    [x.clear() for x in ax];
    ax[0].plot(O2_t, s, '-')
    ax[0].plot(O2_t, spl, '--')
    ax[0].set_ylabel('Signal')
    ax[2].plot(O2_t, Rm, '.r')
    ax[2].plot(O2_t, Rp, '.b')
    ax[2].set_ylabel('Oxygen uptake rate')
    ax[2].set_xlabel('t')
    for j in O2_t[idx]:
        [x.axvline(x=j, linestyle='--', color='k', linewidth=0.5) for x in ax[0:3:2]];
    ax[1].plot(xpos,pos, 'ob')
    ax[1].plot(xpos,np.polyval(O2_R[i,[3,1]], xpos), '-b')
    ax[1].set_ylabel('mean R+')
    ax[3].plot(xneg,neg, 'or')
    ax[3].plot(xneg,np.polyval(O2_R[i,[2,0]], xneg), '-r')
    ax[3].set_ylabel('mean R-')
    ax[3].set_xlabel('t')

    plt.show()
    print('Plot of signal {}'.format(i))
    print('Press key to plot next')
    w = input();

plt.close(1)

#' ## Regress with ARD and check the weights
N = pH.shape[0]
dim_pH = pH_M.shape[1] 
dim_O2 = O2_R.shape[1]
X = np.zeros([N,dim_pH+dim_O2])
X[:,0:dim_pH] = pH_M

X[:,dim_pH:] = O2_R

scale = lambda x: (x - np.mean(x,axis=0)) / np.std(x,axis=0) # zscore
#scale = lambda x: (x - x.min(axis=0)) / (x.max(axis=0) - x.min(axis=0)) # baseline
X = scale(X)

## ARD regression
reg = ARDRegression (n_iter=int(2e3), compute_score=True)
reg.fit(X, NH4)
y_pred = reg.predict(X)

print("Mean squared error: %.2f "
      % mean_squared_error(NH4, y_pred))
# Explained variance score: 1 is perfect prediction
print('Variance score: %.2f '
      % r2_score(NH4, y_pred))

#+ caption= "ARD feature selection"
plt.ion()
plt.figure()
plt.clf()
_n_ = np.arange(len(reg.coef_))
plt.bar(_n_, reg.coef_)
plt.ylabel('Weight in the model')
#name = np.array(['pH\_t\_max', 'pH\_max','O2\_R\_min', 'O2\_R\_mean', 'O2\_R\_max',\
#    'e\_sz\_O2\_R-','e\_sz\_O2\_R+'])
#plt.xticks(_n_, name)
plt.show()

#+ caption= "Regression results."
plt.figure()
plt.clf()
plt.plot(NH4,'o', label='data')
plt.plot(y_pred,'x', label='pred.')
plt.ylabel('NH4')
plt.xlabel('Cycle')
plt.legend()
plt.show()
