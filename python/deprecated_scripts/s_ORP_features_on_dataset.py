# -*- coding: utf-8 -*-
# vim: set tabstop=4
# s_ORP_features_on_dataset.py
#!/usr/bin/env python3
""" The script makes plots to show differences in identification if the
nitrification is finished or not between maintained and unmaintained ORP sensor.
This decision is based on a step change in the aeration pattern."""

# Copyright (C) 2018 Juan Pablo Carbajal
# Copyright (C) 2018 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

#' % Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
#' % Mariane Yvonne Schneider <myschneider@meiru.ch>
#' % 05.07.2018

import platform

import numpy as np

import matplotlib
import matplotlib.pyplot as plt

from scipy.signal import savgol_filter

from SBRdata.utils import (
    Result,
    sensorsdict,
    processrawdata,
    fscore,
    contingencytable
)
from SBRfeatures.ORP_features import aeration_plateau
from SBRfeatures.basic_funcs import smooth
from SBRplots.point_features import (
    feature_vs_value,
    feature_signal_and_output)

matplotlib.rcParams['text.latex.unicode'] = True
matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams['font.sans-serif'] = 'DejaVu Sans'
matplotlib.rcParams.update({'font.size': 12})
if platform.system() != 'Windows':
    matplotlib.rcParams['text.usetex'] = True
    plt.ion()
else:
    pass

# Configure the size of the legend
LEGENDFMT = dict(bbox_to_anchor=(0, 1.02, 1.3, 0.3),
    loc='lower left', mode='expand', ncol=3, fontsize='small')

# Globals to pass values to plot functions
NH4 = np.array([])
NH4ISLOW = np.array([])
NH4THRESHOLD = 1
CYCLENAMES = np.array([])

def plotplateau(*,xval=None, signal=None, output=None, feature=None,
                axes=None, plots=None, case=None, name=None):
    if (axes is None) and (plots is None):
        axes, plots = feature_signal_and_output()
        # Rename singal axes
        axes['signal'].set_xlabel('time')
        axes['signal'].set_ylabel('ORP')
        # Rename output axes
        axes['output'].set_ylabel('Effluent NH4')
        axes['output'].set_ylim(0, NH4.max())
        # add line to plot slope of feature
        plots['feature_slope'], = axes['signal'].plot([0,0], [0,0], 'r-')
        # add line to show threshold in output
        plots['outoput_threshold'] = \
            axes['output'].axhline(y=NH4THRESHOLD, xmin=0, xmax=1, c="black",
                                   linewidth=1, linestyle='--')
        handles, _ = axes['signal'].get_legend_handles_labels()
        axes['signal'].legend(handles=handles, **LEGENDFMT)
    else:
        # Update figure
        axes, plots = feature_signal_and_output(xval=xval,
            signal=signal, output=output, feature=(feature[0], feature[1][0]),
            axes=axes, plots=plots)
        plots['signal'].set_label(name.replace('_', r'\_'))

        # configure markers according to case
        plots['output'].set_color(case.value.fmt['color'])
        plots['feature'].set(**case.value.fmt)

        if feature is None:
            feature = (None, None)
        # Update feature, only time and signal value are used
        # if the feature has more, add it after calling this function
        if feature[0] is not None:
            # draw local slope line
            tv = feature[0]
            yv = feature[1][0]
            dyv = feature[1][1]
            Delt = feature[1][2]

            dtloc = np.array([-0.1,0.1])
            plots['feature_slope'].set_data(tv + dtloc, yv + dyv * dtloc)
            plots['feature_slope'].set_visible(True)
            slope_deg = np.real(np.arctan(dyv/yv)) * 180 / np.pi
            plots['feature'].set_label(
                'plateau: slope {:.1f}°, $\Delta t$ {:.1e}'.format(slope_deg, Delt))
        else:
            plots['feature_slope'].set_visible(False)

        # update legend
        handles, _ = axes['signal'].get_legend_handles_labels()
        axes['signal'].legend(handles=handles, **LEGENDFMT)

        print('Showing %s'%name)
        print(case.name)

    return axes, plots

def applyfeature(t, svalue, *, feature, plotfunc=None):
    """ TODO """
    # Prepare plot
    doplot = False
    if plotfunc is not None:
        doplot = True
        axes, plots = plotfunc()

    # Number of signals
    N = len(svalue)

    # Detect feature and plot
    hasfeature = np.array([False] * N)
    time_feature = np.array([np.nan] * N)
    case = np.array([None] * N)
    for i, signal in enumerate(svalue): # s is all sensor values for one cycle

        tv, sv, ss = feature(t, signal)
        if tv is not None:
            hasfeature[i] = True
            time_feature[i] = tv
        # Result type
        case[i] = Result.classify(hasfeature[i], NH4ISLOW[i])

        # If desired, plot the signals
        if doplot:
            axes, plots = plotfunc(xval=t, \
                signal=(signal, ss), output=NH4[i], feature=(tv, sv),
                axes=axes, plots=plots, case=case[i], name=CYCLENAMES[i])

            plt.pause(0.1) # Hack to allow thread synchronization in Windows systems
            key = input('Press q then return to quit, just return to continue: ')
            if key == 'q':
                quit()

    if doplot:
        plt.close()

    return time_feature, hasfeature, case

if __name__ == '__main__':
    import argparse
    from  functools import partial

    ## Parse command line arguments
    parser = argparse.ArgumentParser(description='Run analysis on all ORP dataset.')
    parser.add_argument(
        '-p', '--plot', help='If present, plots each singal in the dataset.',
        action='store_true')
    args = parser.parse_args()

    plotfunc = None
    if args.plot:
        plotfunc = plotplateau

    sensorsource = sensorsdict(stype='ORP')
    NH4, nonan, sensor = processrawdata(filesdict=sensorsource)
    NH4ISLOW = (NH4 <= NH4THRESHOLD)

    # feature function
    slope_tol = np.tan(30 * np.pi / 180)
    feature_func = {
        'maintained':partial(aeration_plateau,
            slope_tol=slope_tol, smoother=partial(smooth, order=2, freq=3)),
        'unmaintained':partial(aeration_plateau,
            slope_tol=slope_tol, smoother=partial(smooth, order=2, freq=3))
        }

    hasfeature = dict().fromkeys(sensorsource.keys())
    time_feature = dict().fromkeys(sensorsource.keys())
    cases = dict().fromkeys(sensorsource.keys())
    for sname, s in sensor.items():
        print('** Sensor type: %s'%sname)

        CYCLENAMES = s.cycle_names()
        ## Filter inputs that do not show O2 aeration valley
        ## Select aeration phase
        svalue = s.interp_nan(phase=4)[nonan, :]
        # time as phase completion 0==start, 1==end
        t = s.completion_phase(4)

        time_feature[sname], hasfeature[sname], cases[sname] = applyfeature(t, svalue,
            plotfunc=plotfunc, feature=feature_func[sname])

        TFtable = contingencytable(cases[sname])
        ## Print summary of classification
        for k, v in TFtable.items():
            print('{}:\t{}\t({})'.format(Result[k].value.text, v, k))

        for beta in [0.5, 1, 2]:
            print('F({:.1f})-score: {:.2f}'.format(beta, fscore(TFtable, beta**2)))

    masks = dict().fromkeys(sensor.keys())
    for k, v in time_feature.items():
        masks[k] = (cases[k] == Result.FALSE_POSITIVE) \
                 | (cases[k] == Result.FALSE_NEGATIVE)
    phase_completion = {k : v * 100 for k, v in time_feature.items()}

    fig, axes, _ = feature_vs_value(phase_completion, \
        NH4, color_mask=masks)

    axes[0].axvline(x=NH4THRESHOLD, ymax=1.27, c='k', linestyle='--', zorder=0, \
        clip_on=False)

    axes[0].set_xscale('log')
    axes[0].axis('tight')
    axes[0].set_xlabel('Effluent Ammonium Nitrogen [mg/L]')
    axes[0].set_ylabel(r'Aeration Phase Completion [\%]')

    plt.legend(bbox_to_anchor=(0.1, 1.24, 1, 0.2), loc="upper left", ncol=2)
    plt.show(block=True)
