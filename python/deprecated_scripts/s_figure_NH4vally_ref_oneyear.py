# -*- coding: utf-8 -*-
# vim: set tabstop=4
# s_figure_binary_nh4_boxplot_subplot.py
#!/usr/bin/env python3

# Copyright (C) 2018 Juan Pablo Carbajal
# Copyright (C) 2018 Maraine Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

#' % Ammonium valley identification using maintained and unmaintained pH sensors
#' % Maraine Yvonne Schneider <myschneider@meiru.ch>
#' % 20.06.2018

#' The script plots to show differences in identification of ammonium valley
#' between maintained and unmaintained pH sensors.

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.legend_handler import HandlerLine2D

import numpy as np

import platform
if platform.system() != 'Windows':
    matplotlib.rcParams['text.usetex'] = True
else:
    pass
matplotlib.rcParams['text.latex.unicode'] = True
matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams['font.sans-serif'] = 'DejaVu Sans'
matplotlib.rcParams.update({'font.size': 14})

from scipy import signal

from sklearn.decomposition import PCA

from SBRdata.dataparser import IOData, SensorData
from pH_features import aeration_valley
from plots.plot import savefigure, threashold

print('skript is running...')

savef = 0               # choose between 1 and 0 depening if the figure should
                        # be saved automatically or not
fontsize = 14           # Larger Fontsize for plots

datapath = '../data/'
files = {'maintained':'AI4_MW.csv', 'unmaintained':'AI4_MW.csv'}
sensor_names = list(files.keys())

#' ## Data load and pre-process
#' First load the data, filter NA values and select the output to regress.
#' In this case we use the NH4 value at the effluent.
sensor = dict()
_NH4 = np.array([])
for signal, filenam in files.items():
    filename = datapath + filenam
    print('Processing signal {} from {}'.format(signal, filename))
    sensor[signal] = SensorData(filename)

Ydata = IOData(datapath + files[sensor_names[0]])
if not _NH4.size > 0:
    _NH4 = Ydata.NH4[:, 1]

    # filter NA values in output
    nonan = np.isfinite(_NH4)
    testNH4 = _NH4
    _NH4 = _NH4[nonan]

# Check allfiles have the same output values
if not np.allclose(_NH4, Ydata.NH4[nonan, 1]):
    raise ValueError('Output signals differ!')

## Compute amonia valley for pH sensor 1 and 2
fp_valley_opt = {'order':5}
s_valley_opt = {'freq':5}

dateT = dict().fromkeys(sensor_names)
aerphase = dict().fromkeys(sensor_names)
value = dict().fromkeys(sensor_names)
t = dict().fromkeys(sensor_names)
dt = dict().fromkeys(sensor_names)
valley = dict().fromkeys(sensor_names)
novalley = dict().fromkeys(sensor_names)
time_valley = dict().fromkeys(sensor_names)
pH_novalley = dict().fromkeys(sensor_names)
pH_valley = dict().fromkeys(sensor_names)
pca = dict().fromkeys(sensor_names)
for sname in sensor_names:
    dateT[sname] = np.array(sensor[sname].cycles_datetime())[nonan]
    ## Filter inputs that do not show pH aeration valley
    ## Select aeration phase
    aerphase[sname] = np.nonzero(sensor[sname].phase == 4)[0]
    value[sname] = sensor[sname].interp_nan()[nonan, :][:, aerphase[sname]]
    # time as phase completion 0==start, 1==end
    t[sname] = sensor[sname].time[aerphase[sname]]
    t[sname] = (t[sname] - t[sname][0]) / (t[sname][-1] - t[sname][0])
    dt[sname] = t[sname][1] - t[sname][0]

    # Detect amonia valley and plot
    novalley[sname] = []
    valley[sname] = []
    time_valley[sname] = []
    for s in value[sname]: # s is all pH values for one cycle
        m, s_smooth = aeration_valley(t[sname], s, \
                               smooth_opt=s_valley_opt, \
                               findpeaks_opt=fp_valley_opt)
        if m:
            novalley[sname].append(False)
            valley[sname].append(True)
            time_valley[sname].append(m)
        else:
            novalley[sname].append(True)
            valley[sname].append(False)

    novalley[sname] = np.array(novalley[sname])
    valley[sname] = np.array(valley[sname])
    time_valley[sname] = np.array(time_valley[sname])
    pH_novalley[sname] = value[sname][novalley[sname], :]
    pH_valley[sname] = value[sname][valley[sname], :]

    print("Number of signals with amonia valley (maintained): {}/{}".format(\
        np.sum(np.logical_not(novalley[sname])), value[sname].shape[0]))


    #' ## PCR training
    #' We extract PCs from the whole dataset
    pca[sname] = PCA(n_components=7, svd_solver='full')
    pca[sname].fit(pH_valley[sname])

    print('Number of Principal components for reference sensors: %.2f'
          % pca[sname].n_components_)

#' ## Plot results
#+ caption='This figure is so cool!'
sname = 'maintained'

fig, ax = plt.subplots(nrows=1, ncols=1)
ax.set_title('Ammonium valley observed?')
# ax.set_ylabel('Ammonium valley observed?')
ax.set_xlabel('date')
threash = 0.6
valley_above, valley_below = threashold(threash, _NH4, valley[sname])
dateT_above, dateT_below = threashold(threash, _NH4, dateT[sname])

line1, = ax.plot(dateT_above, valley_above, 'ro', \
  markersize=8, label='$>$ 0.6 $mg_{NH_4-N}/L$')
line2, = ax.plot(dateT_below, valley_below, 'go', \
  markersize=8, label='$<=$ 0.6 $mg_{NH_4-N}/L$')
for i, nh4 in enumerate(_NH4):
    if nh4 < 0.6:
        ax.plot(dateT[sname][i], valley[sname][i], 'go', \
            linewidth=4, markersize=7)
    if nh4 >= 0.6:
        ax.plot(dateT[sname][i], valley[sname][i], 'ro', \
            linewidth=4, markersize=7)

ax.legend(handler_map={
    line1: HandlerLine2D(numpoints=1), \
    line2: HandlerLine2D(numpoints=1)
    }, loc='best', ncol=1)
labels = ['0', 'False', 'True']
ax.set_yticklabels(labels)
savefigure(savef, fig, 'time_all_un-maintained')
# if savef == 1:
#     fig.savefig('../figures/timeline_all_un-maintained.png')
#     fig.savefig('../figures/timeline_all_un-maintained.eps', format='eps', dpi=1000)
# else:
#     pass

plt.show()
