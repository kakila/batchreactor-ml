# vim: set tabstop=4
# s_pH_ROCcurve.py
#!/usr/bin/env python3
""" The script plots the ROC curve of the pH sensors and saves .pkl containing
the results for the different cutoff frequencies."""

# Copyright (C) 2018 Mariane Yvonne Schneider
# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 05.07.2018

############
## Imports
# built-ins
import platform

# 3rd party
from  functools import partial
from joblib import (
    Parallel,
    delayed)                # for parallel loops
import matplotlib
import matplotlib.pyplot as plt
import multiprocessing      # for parallel loops
import numpy as np
import pickle
from sbrfeatures.features_pH import aeration_valley
from sbrfeatures.basicfunctions import smooth

# user
from SBRdata.utils import (
    Result,
    sensorsdict,
    processrawdata,
    fscore,
    contingencytable
)
from SBRplots.roc import ROCcurve as pltROC

############

matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams.update({'font.size': 12})
if platform.system() != 'Windows':
    matplotlib.rcParams['font.sans-serif'] = 'DejaVu Sans'
else:
    matplotlib.rcParams['font.sans-serif'] = 'Arial'

# number of CPUs
NUMCORES = multiprocessing.cpu_count()

# Configure the size of the legend
LEGENDFMT = dict(bbox_to_anchor=(0, 1.02, 1.3, 0.3),
                 loc='lower left', mode='expand', ncol=3, fontsize='small')

# Globals to pass values to plot functions
NH4 = np.array([])
NH4ISLOW = np.array([])
NH4THRESHOLD = 1

def applyfeature(time, svalue, *, feature):
    """
    Apply a feature to a signal.

    Arguments
    ---------
    time : array_like
        Time values.
    signal : array_like
        Signal values of the same length as t.

    Keyword arguments
    -----------------
    feature : function, optional
        Is a function which can identify a feature.

    Returns
    -------
    array_like of float
        Time of feature occurance.
    array_like of bool
        Indicating if the feature has been found (True) or not (False).
    array_like of tuple
        Classification based on control measurements and the occurance of the
        feature.
    """
    # Number of signals
    N = len(svalue)

    # Detect feature and plot
    hasfeature = np.array([False] * N)
    time_feature = np.array([np.nan] * N)
    case = np.array([None] * N)
    for i, signal in enumerate(svalue): # s is all sensor values for one cycle
        tv, sv, ss = feature(time, signal)
        if tv is not None:
            hasfeature[i] = True
            time_feature[i] = tv
        # Result type
        case[i] = Result.classify(hasfeature[i], NH4ISLOW[i])
    return time_feature, hasfeature, case

def getdata(sensor):
    """
    Gets the data of the sensor signals.

    Arguments
    ---------
    sensor : array_like
        Data from online sensors.

    Keyword arguments
    -----------------
    None

    Returns
    -------
    dict
        Time of measurement.
    dict
        Measurement value.
    """
    svalue = dict().fromkeys(sensor.keys())
    time = dict().fromkeys(sensor.keys())
    for sname, s in sensor.items():
        print('** Sensor type: %s'%sname)

        # Select aeration phase and filter inputs without NH4 effluent measurements
        svalue[sname] = s.interp_nan(phase=4)[nonan, :]
        # time as phase completion 0==start, 1==end
        time[sname] = s.completion_phase(4)

    return time, svalue

def doROC(time, svalue, param, featfunc):
    """
    Applies the ROC curve.

    Arguments
    ---------
    time : array_like
        Time values.
    signal : array_like
        Signal values of the same length as t.
    param : array_like
        Space of parameter values to be used for the optimisation.
    featfunc : function
        Function applied with the given parameter.

    Keyword arguments
    -----------------
    None

    Returns
    -------
    tuple
        With the contingencytable of all TRUE and FALSE cases and the ROC for
        the different parameter.
    """
    _, _, cases = applyfeature(time, svalue, feature=featfunc)

    table = contingencytable(cases)
    roc = []
    roc.append(table[Result.FALSE_POSITIVE.name] \
        / (table[Result.TRUE_NEGATIVE.name] \
         + table[Result.FALSE_POSITIVE.name]))
    roc.append(table[Result.TRUE_POSITIVE.name] \
        / (table[Result.TRUE_POSITIVE.name] \
         + table[Result.FALSE_NEGATIVE.name]))

    return (table, roc)

def featurefunctiongen(param):
    """
    Generates the feature function.

    Arguments
    ---------
    param : array_like
        Space of parameter values to be used for the optimisation.

    Keyword arguments
    -----------------
    None

    Returns
    -------
    function
        Returns a partial function with the feature, as well as the passed
        parameter.
    """
    tminmax = [0.1, 0.99] # time interval in which to look for valley
    return partial(aeration_valley,
        t_interval=tminmax, smoother=partial(smooth, order=2, freq=param))

if __name__ == '__main__':
    sensorsource = sensorsdict(stype='pH')
    NH4, nonan, sensor = processrawdata(filesdict=sensorsource)
    NH4ISLOW = (NH4 <= NH4THRESHOLD)

    parameters = np.concatenate((np.linspace(0.1, 1.5, 15),
                                np.linspace(1.5, 3.0, 15),
                                np.linspace(3.0, 20.0, 10)))
    print('Grid search over %d parameter values ...'%len(parameters))

    # collect data for all sensors
    tdict, sdict = getdata(sensor)

    TFtable = dict().fromkeys(sensor.keys())
    ROC = dict().fromkeys(sensor.keys())
    paramstr = dict().fromkeys(sensor.keys())
    
    parallelrunner = Parallel(n_jobs=NUMCORES) # executes jobs in parallel
    for sname in sensor.keys():
        print('** Sensor type: %s'%sname)

        results = parallelrunner(
                delayed(doROC)(tdict[sname], sdict[sname], param,
                featurefunctiongen(param)) for param in parameters
                )

        TFtable[sname], roc = zip(*results)
        ROC[sname] = np.array(roc, ndmin=2)
        paramstr[sname] = sname.replace('_', ' ')

    plt.figure(1)
    plt.clf()
    pltROC(ROC, paramstr)

    plt.show()

    params = (parameters,)
    param_names = ('cutoff frequency',)
    outfile = '../data/pH_Tables.pkl'
    print('Saving contingengy tables to %s ...'%outfile)
    with open(outfile, 'wb') as fid:
        pickle.dump(TFtable, fid)
        pickle.dump(params, fid)
        pickle.dump(param_names, fid)

    outfile = '../data/pH_ROC.pkl'
    print('Saving ROC curves to %s ...'%outfile)
    with open(outfile, 'wb') as fid:
        pickle.dump(ROC, fid)
        pickle.dump(params, fid)
        pickle.dump(param_names, fid)
