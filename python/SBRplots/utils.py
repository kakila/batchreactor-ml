# -*- coding: utf-8 -*-
# vim: set tabstop=4
# utils.py
#!/usr/bin/env python3

# Copyright (C) 2018 Maraine Yvonne Schneider
# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Maraine Yvonne Schneider <myschneider@meiru.ch>
# Created: 26.06.2018
from pathlib import PurePath

def threashold(threasholdvalue, measurement, feature):
    '''For the separation of the data in above and below a certain threadhold
    value. E.g. to separate based on the measured ammonium concentration.'''
    above = feature[measurement > threasholdvalue]
    below = feature[measurement <= threasholdvalue]
    return above, below

FIG_FMT_OPT = {
    'png':{},
    'eps':dict(dpi=1000)
}

def savefigure(fig, *, name='figure', path='.', format='png'):
    '''Save figures to PNG and EPS using the provided data'''
    figname = PurePath(path, name+'.'+format)
    fig.savefig(figname, format=format, **FIG_FMT_OPT[format])
    print('Saved to %s'%figname)
