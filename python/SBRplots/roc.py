#!/usr/bin/env python3
# roc.py
# vim: set tabstop=4

''' plots the ROC curve '''

# Copyright (C) 2018 Juan Pablo Carbajal
# Copyright (C) 2018 Maraine Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Created: 20.08.2018
import matplotlib.pyplot as plt

def ROCcurve(ROC, strs):
    for sensor, c in ROC.items():
        for i, _ in enumerate(ROC):
            plt.plot(c[:, 0], c[:, 1], marker='o', markerfacecolor='None',
                     linestyle='None', markersize=3*(len(ROC)-i))

    plt.xlabel('FPR')
    plt.ylabel('TPR')
    plt.legend(strs.values(), loc="best")
