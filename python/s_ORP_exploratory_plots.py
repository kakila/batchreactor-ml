# -*- coding: utf-8 -*-
# vim: set tabstop=4
# s_ORP_exploratory_plots.py
#!/usr/bin/env python3
""" The script produces exploratory plots to see the differences between
ORP sensors."""

# Copyright (C) 2018 Juan Pablo Carbajal
# Copyright (C) 2018 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 19.09.2018

import platform

import matplotlib
import matplotlib.pyplot as plt

import numpy as np

from scipy.signal import savgol_filter

import seaborn as sns

from sklearn.decomposition import PCA

from SBRdata.utils import(
    sensorsdict,
    processrawdata,
    )

matplotlib.rcParams['text.latex.unicode'] = True
matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams['font.sans-serif'] = 'DejaVu Sans'
matplotlib.rcParams.update({'font.size': 12})
if platform.system() != 'Windows':
    matplotlib.rcParams['text.usetex'] = True
    #plt.ion()
else:
    pass

NH4THRESHOLD = 1

if __name__ == '__main__':

    sensorsource = sensorsdict(stype='ORP')
    NH4, nonan, sensor = processrawdata(filesdict=sensorsource)
    NH4ISLOW = (NH4 <= NH4THRESHOLD)
    ALPHAS = NH4ISLOW*0.5
    rgba_colors = np.zeros(len(ALPHAS))

    meanvalue = dict().fromkeys(sensorsource.keys())
    stdvalue = dict().fromkeys(sensorsource.keys())
    meanvalue_s = dict().fromkeys(sensorsource.keys())
    stdvalue_s = dict().fromkeys(sensorsource.keys())
    princomp = dict().fromkeys(sensorsource.keys())
    paramstr = dict().fromkeys(sensor.keys())
    # number of principal components
    nc = 2
    pcanalizer = PCA(n_components=nc, whiten=True, svd_solver='full')
    for sname, s in sensor.items():
        print('** Sensor type: %s'%sname)

        ## Select aeration phase
        svalue = s.interp_nan(phase=4)[nonan, :]
        t = s.completion_phase(4)
        ## Summary statistics singals
        meanvalue[sname] = svalue.mean(axis=0)
        stdvalue[sname] = svalue.std(axis=0)
        ## Summary statistics residual singals
        ss = savgol_filter(svalue, 7, 1, axis=1)
        # Uncomment to plot each signal and smoothed
        '''
        for i,sig in enumerate(svalue):
            plt.clf()
            plt.plot(t, sig, '.', t, ss[i,:], '-');
            plt.show()
        '''
        residual = svalue - ss
        meanvalue_s[sname] = residual.mean(axis=1)
        stdvalue_s[sname] = residual.std(axis=1)
        ## PCA
        pcanalizer.fit(svalue)
        princomp[sname] = pcanalizer.transform(svalue)
        print(('Explained varinace ratio: ' + '%.2f '*nc) % tuple(
            pcanalizer.explained_variance_ratio_))
        ## replacing the _ in the sensorname for the label of the figure.
        paramstr[sname] = sname.replace('_', ' ')

    matplotlib.style.use('seaborn')
    fig, axes = plt.subplots(1, 3)
    for i, nameMS in enumerate(zip(meanvalue.keys(), meanvalue.values(),
                                   stdvalue.values())):
        axes[0].scatter(nameMS[1], nameMS[2], c='C%d'%i, edgecolor='k',
                        label=nameMS[0],
                        s=np.linspace(2, 10, len(nameMS[1]))**2,
                        alpha=0.2)
    axes[0].text(0, 0, 'A', transform=axes[0].transAxes,
            size=20, weight='bold')
    axes[0].set_xlabel('mean')
    axes[0].set_ylabel('standard deviation')
    axes[0].set_xticklabels([])
    axes[0].set_yticklabels([])

    for i, nameMS in enumerate(zip(meanvalue_s.keys(), meanvalue_s.values(),
                                   stdvalue_s.values())):
        axes[1].scatter(nameMS[1], nameMS[2], c='C%d'%i, edgecolor='k',
                        label=nameMS[0])
    axes[1].text(0, 0, 'B', transform=axes[1].transAxes,
            size=20, weight='bold')
    axes[1].set_xlabel('mean of residuals')
    axes[1].set_ylabel('standard deviation of residuals')
    axes[1].set_yscale('log')
    axes[1].set_ylim(bottom=1e-1)
    axes[1].set_xticklabels([])
    axes[1].set_yticklabels([])

    for i, nameX in enumerate(princomp.items()):
        sname = nameX[0]
        X = nameX[1]
        axes[2].scatter(X[:, 0], X[:, 1], c='C%d'%i, edgecolor='k', label=sname)

    axes[2].text(0, 0, 'C', transform=axes[2].transAxes,
            size=20, weight='bold')
    axes[2].set_xlabel('principal component 1')
    axes[2].set_ylabel('principal component 2')
    axes[2].set_xticklabels([])
    axes[2].set_yticklabels([])

    plt.tight_layout(pad=2)
    plt.legend(paramstr.values(), bbox_to_anchor=(-1.8, 0.87, 1, 0.2),
               loc="upper left", ncol=len(sensor))
    plt.show()
