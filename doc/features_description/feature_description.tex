\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}

\usepackage{natbib}
\usepackage{graphicx}
\usepackage[small]{subcaption}

\usepackage{minted}

\usepackage[hyphens]{url}
\usepackage[hidelinks]{hyperref}
\hypersetup{breaklinks=true}

\usepackage{enumitem}

\graphicspath{{../img}}

\author{Juan Pablo Carbajal}
\title{Description of the methods used to compute signal features}

\begin{document}
\maketitle

\begin{abstract}
The algorithms to compute the signal features used in this study were kept as basic as possible.
The authors spend considerable time searching, without success, for available algorithms for the computation of these features and the corresponding code (under adequate\footnote{a license allowing copying, modification, and redistribution; e.g. GPLv3 compatible} license).
Hence our code appears as a major contribution to the community and should provide a basis for quantitative and objective evaluations of the features and methods.
We make no claims about the optimality of the solutions implemented in our code.

All the methods are implemented using Python 3 and a module is available for download~\citep{pysbrfeatures}.
The module builds up on the functionality provided by scipy~\citep{scipy}.
\end{abstract}

\section{General structure of feature computation}
All computed features are phase specific.
There are point (or local) features that take a signal and return a $n$-dimensional point and there are functional (or non-local features) that take a signal and return another signal (a $d$-dimensional vector where $d$ is not specified).

The structure of the algorithms for all features is the following:

  \begin{enumerate}[label=(\roman*)]
    \item Select phase and/or time interval
    \item \label{it:filters}Filter signal
    \item \label{it:features}Compute feature
  \end{enumerate}

In the subsequent sections we provide details about the filters we used in step~\ref{it:filters} (sec.~\ref{sec:filters}), and the
computation of features in step~\ref{it:features} (sec.~\ref{sec:features}).

\section{Digital filters\label{sec:filters}}

There three reasons for a filtering stage: i) signals have noise, ii) during the aeration phase oscillations are induced in the signal by the bang-bang operation of the air valve, iii) most features require a degree of regularity in the representation of the signal, e.g. continuous time derivatives.

The algorithms implementing our features are agnostic as to what filter the user wants to use.
The user needs only to provide a function with the signature
\mintinline{python}|filtered_signal = smoother (time_vector, signal)|
where
\mintinline{python}|signal|, \mintinline{python}|time_vector|, and \mintinline{python}|filtered_signal|
are the original sensor signal, its time vector, and the corresponding filtered signal, respectively; the
\mintinline{python}|smoother|
is here just a place holder for the user defined function.

The filters we used and tested in our study are
\begin{itemize}
  \item \textbf{Butterworth}:
  these are frequency based filters.
  They remove components in a selected region of the Fourier spectrum, in our case this corresponds to all frequencies below a cutoff frequency (low-pass).
  \item \textbf{Savitzky-Golay} (SG): these are time domain filters.
  they smooth the signal by fitting a polynomial of given order to a local neighborhood of each point.
\end{itemize}

Butterworth filters were very useful to remove the oscillations induced by the aeration method.
The users might consider using a matching filter for better performance, this remains as future work.
SG filters were used with signals that did not present the induced oscillations, e.g. ORP signals.
SG filters better conserve the shape of the signal when compared to Butterworth filters, hence are not so useful to remove spurious signal variation in a given frequency band.

\section{\label{sec:features}Signal features}
Features accept a hierarchy, basic features are general properties of signals (e.g. extrema, inflection points, etc., see sec.~\ref{sec:basicfea}).
Sensor specific features combine or select basic features to make them specific for the particular signal analyzed (e.g. pH valley during aeration phase, see sec.~\ref{sec:sensorfea}).

Most of the calculations are based on the assumption that the signals have, at least,
their first derivative continuous in the interval of the analyzed phase (e.g. aeration), i.e. $C^1([0,1])$.
Some features, however, assume more regularity.
Hence, features exploit spline representations of the smoothed signals (see sec.~\ref{sec:filters}).
These splines have, at least, continuous derivatives up to order 2.
This decision, implies a underlying ODE model of order at least 2, since during the aeration phase the input to the system is approximately discontinuous (bang-bang operation of a air valve).
These assumptions are compatible with the mean trajectories of the model used in~\citep{Thurlimann2017}.

\subsection{Basic point features\label{sec:basicfea}}
All features used for the current study relay on two basic features:
\begin{enumerate}[label=(\roman*)]
  \item\label{it:extrema} \textbf{Signal extrema}:
are defined as the points where the signal attains its maxima or its minima.
Neither splines nor derivatives are involved in this calculation.
  \item\label{it:pcextrema} \textbf{Signal extrema via critical points}:
are defined as points at which the first derivative is zero and the second derivative is non-zero.
Extrema are computed on a spline model of the signal.
The computation is exact if the degree if the spline allows it, otherwise it uses a root finding algorithm.
  \item\label{it:infpoints} \textbf{Inflection points}:
are defined as points at which the second derivative is zero and the lowest-order (above the second) non-zero derivative is odd (e.g. third, fifth, etc.).
If additionally the first derivative is zero, then the point is called a saddle.
In our module we compute inflection points based on spline models of the filtered data.
\end{enumerate}

\subsection{Sensor specific features\label{sec:sensorfea}}

  \subsection{pH}
    The pH featured used in this study is called \mintinline{python}|aeration_valley|.
    The valley is a global minimum of the pH signal during the aeration phase.
    The minimum is first searched using critical points (item~\ref{sec:basicfea}~\ref{it:pcextrema}), if none is found the signal extrema are used (item~\ref{sec:basicfea}~\ref{it:extrema}).
    This feature is 2-dimensional point composed of the time at which the minimum was found, and the corresponding value of the signal.

  See Fig.~\ref{fig:pHfea} for an example in a synthetic signal

    \begin{figure}[htpb]
      \includegraphics[draft, width=\textwidth, height=0.3\textheight]{pHfeatures_showcase.pdf}
      \caption{\label{fig:pHfea} Example of feature in model signal}
    \end{figure}

  \subsection{Dissolved oxygen}
    The DO featured used in this study is called \mintinline{python}|aeration_ramp|.
    The ramp is a maximum in the derivative of the signal during the aeration phase.
    The maximum is selected among the union of the non-saddle inflection points (item~\ref{sec:basicfea}~\ref{it:infpoints}) and the maxima of the signal's derivative (item~\ref{sec:basicfea}~\ref{it:pcextrema})
    The feature is a 3-dimensional point composed of the time of occurrence of the ramp, and the value of the signal and its derivative at that time.

    See Fig.~\ref{fig:DOfea} for an example in a synthetic signal

    \begin{figure}[htpb]
      \includegraphics[draft,  width=\textwidth, height=0.3\textheight]{DOfeatures_showcase.pdf}
      \caption{\label{fig:DOfea} Example of feature in model signal}
    \end{figure}

  \subsection{Oxidation reduction potential}
    The ORP featured used in this study is called \mintinline{python}|aeration_plateau|.
    The plateau is defined as low absolute values of the derivative of the signal in a connected interval.
    This is not a point feature, it processed a signal and returns an interval.
    We map the interval to the 4-dimensional point composed of the mid point of the interval, the value of the signal at the mid point, the mean derivative in the interval, and the length of the interval.

    See Fig.~\ref{fig:ORPfea} for an example in a synthetic signal

    \begin{figure}[htpb]
      \includegraphics[draft,  width=\textwidth, height=0.3\textheight]{ORPfeatures_showcase.pdf}
      \caption{\label{fig:ORPfea} Example of feature in model signal}
    \end{figure}

\bibliographystyle{unsrtnat}
\bibliography{../references}
\end{document}
