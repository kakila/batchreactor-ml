# Batch reactor feature based regression

Scripts to reproduce the analyses of batch reactors signals reported in the article [Beyond signal quality: The value of unmaintained pH, dissolved oxygen, and oxidation-reduction potential sensors for remote performance monitoring of on-site sequencing batch reactors](https://engrxiv.org/ndm7f/)

## Authors

* **Juan Pablo Carbajal**
* **Mariane Schneider**

## License

This project is licensed under the GPLv3+ license - see the [LICENSE](LICENSE) file for details

## Repository structure
The folder structure of the repository is:

    .
    ├── data
    ├── doc
    └── python

The `data` folder is meant to store raw data and the output of scripts.
It is actually a placeholder, no data should be added to the repository.
The data was used for the corresponding publication can be found here:
 [![DOI](https://zenodo.org/badge/DOI/10.25678/0000dd.svg)](https://doi.org/10.25678/0000dd)


The `doc` folder contains documents used during the development of this package.
They do not contain actual documentation; to get documentation please refer to the [article](https://engrxiv.org/ndm7f/).

The `python` folder contains scripts implementing the analysis of the data using the features from the [sbrfeatures](https://gitlab.com/sbrml/sbrfeatures) module.

## How to create the figures from the article?
The scripts can be found in the python folder. The data needs to be downloaded first and saved into the data folder.
* s_O2_ROCcurve.py creates the DO_Tables.pkl and DO_ROC.pkl needed for s_fig_TFtable DO.
* s_O2_features_on_dataset.py can be run as is to see the overview of the 
  prediction for all cycles. By calling python s_O2_features_on_dataset.py [-p or -plot] every individual cycle is plotted.
* s_ORP_exploratory_plots.py can be run as is and creates the exploratory plot for the ORP signal.
* s_fig_fscore.py needs the addition of DO or pH after calling the script.
* s_pH_features_on_dataset.py creates the same plot as s_O2_features_on_dataset.py and has the same option [-p or -plot].
* s_pH_ROCcurve.py creates the pH_Tables.pkl and pH_ROC.pkl needed for s_fig_TFtable pH.
* s_fig_TFtable can be called with DO as an input. The corresponding ROCcurve script needs to be called first. 